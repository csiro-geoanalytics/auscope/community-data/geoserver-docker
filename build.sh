#!/bin/sh

MAJOR=2
MINOR=17
BUGFIX=0


# Build Geoserver
echo "Building GeoServer ${MAJOR}.${MINOR}.${BUGFIX} "

docker build --build-arg GS_VERSION=${MAJOR}.${MINOR}.${BUGFIX} -t  docker-registry.it.csiro.au/auscope-community/geoserver:latest .

echo "Pushing Geoserver to repository"

docker push docker-registry.it.csiro.au/auscope-community/geoserver:latest

# Build Arguments - To change the defaults when building the image
#need to specify a different value.

#--build-arg WAR_URL=http://downloads.sourceforge.net/project/geoserver/GeoServer/<GS_VERSION>/geoserver-<GS_VERSION>-war.zip





